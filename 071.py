m, n = 3, 7
x, y = 0, 1

for b in range(2, 10 ** 6 + 1):
	a = int(1.0 * m / n * b) + 2
	while a * n >= m * b:
		a -= 1
	
	if a * y > x * b:
		x, y = a, b

print(x, y)