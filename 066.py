from math import sqrt, gcd

def is_square(x):
	sqrtx = int(sqrt(x))
	return x == sqrtx * sqrtx

def get_min_x(d):
	def val(t, sqrtd):
		return (t[0] + t[1] * sqrtd) / t[2]
	
	sqrtd = sqrt(d)
	a, A = [], []
	
	A.append((0, 1, 1))
	a.append(int(val(A[-1], sqrtd)))
	
	p = [0, 1]
	q = [1, 0]
	
	# cnt = 1
	while True:
		p.append(a[-1] * p[-1] + p[-2])
		q.append(a[-1] * q[-1] + q[-2])
		
		t = A[-1]
		aux = t[0] - a[-1] * t[2]
		v1, v2, v3 = t[2] * aux, -t[2] * t[1], aux ** 2 - d * t[1] ** 2
		g = gcd(v1, gcd(v2, v3))
		A.append((v1 // g, v2 // g, v3 // g))
		a.append(int(val(A[-1], sqrtd)))
		
		print(v1, v2, v3, g)
		# print(a[-1], p[-1], q[-1], p[-1] / q[-1], A[-1], gcd(A[-1][0], gcd(A[-1][1], A[-1][2])))
		
		if p[-1] * p[-1] - d * q[-1] * q[-1] == 1:
			return p[-1]
		
		# cnt += 1
		# if cnt == 100:
		# 	break

solx, sold = 0, 0
for d in range(1, 101):
	print(d)
	
	if is_square(d):
		continue
	
	x = get_min_x(d)
	if x > solx:
		solx, sold = x, d

print(solx, sold)