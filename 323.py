from math import factorial as fact
from random import randint

def comb(n, k):
	return fact(n) / fact(k) / fact(n - k)

def simulate():
	V = (1 << 32) - 1
	curr, step = 0, 0
	while curr < V:
		step += 1
		curr |= randint(0, V - 1)
	
	return step

def brute():
	nr, s = 0, 0
	while True:
		nr += 1
		s += simulate()
		print(s / nr)

#brute()
#exit(0)

EPS = 1e-11

dp = [0] * 33
dp[32] = 1

i, last, ans = 0, 0, 0
while dp[0] < 1 - EPS and i <= 20:
	ans += i * (dp[0] - last)
	last = dp[0]

	new_dp = [0.0] * 33
	for nr in range(33):
		for s in range(nr + 1):
			prob = comb(nr, s) / (1 << nr)
			new_dp[nr - s] += dp[nr] * prob
	
	dp = new_dp
	i += 1

print("steps", i)
print(ans)

