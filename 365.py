from util_math import sieve
from itertools import combinations

def crt(vr, vm, inv_matr):
	# solves the equations x = r[i] (mod m[i]) assuming m[i] are pairwise coprime
	if (not vr) or len(vr) != len(vm):
		raise ValueError("Invalid arguments r = {}, m = {}".format(vr, vm))
	
	prodm = 1
	for m in vm:
		prodm *= m
	
	x = 0
	for r, m in zip(vr, vm):
		mm = prodm // m
		c = r
		for r2, m2 in zip(vr, vm):
			if m2 != m:
				c = (c * inv_matr[m][m2]) % m
		#assert c == (r * pow(mm, m - 2, m)) % m

		x += c * mm
	
	return x % prodm

f, inv = [], []

def small_binomial(n, k, p):
	if k > n:
		return 0

	return (f[p][n] * inv[p][k] * inv[p][n - k]) % p

def binomial2(n, k, p):
	res = 1
	while n > 0:
		res = (res * small_binomial(n % p, k % p, p)) % p
		n, k = n // p, k // p
	
	return res

def binomial(n, k, p, q, r):
	return crt([binomial2(n, k, p), binomial2(n, k, q), binomial2(n, k, r)], [p, q, r])

V = 5000
n, k = 10 ** 18, 10 ** 9

is_prime, primes = sieve(V)

primes = [p for p in primes if 1000 < p and p < 5000]
print("nr primes", len(primes))

for i in range(V):
	f.append([1])
	inv.append([0] * V)
	if not is_prime[i]:
		continue

	for j in range(1, V):
		f[i].append((f[i][j - 1] * j) % i)
	
	inv[i][i - 1] = pow(f[i][i - 1], i - 2, i)
	for j in range(i - 2, -1, -1):
		inv[i][j] = (inv[i][j + 1] * (j + 1)) % i

inv_matr = [[0] * V for _ in range(V)]
for i in range(V):
	if not is_prime[i]:
		continue
	
	for j in range(V):
		inv_matr[i][j] = pow(j, i - 2, i)

rests = [(p, binomial2(n, k, p)) for p in primes]

ans = 0
for (p, rp), (q, rq), (r, rr) in combinations(rests, 3):
	ans += crt([rp, rq, rr], [p, q, r], inv_matr)

print(ans)
