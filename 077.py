from util_math import sieve

V = 10000

is_prime, primes = sieve(V)
dp = []
dp.append([1] * len(primes))

i = 1
while True:
	dp.append([0] * len(primes))
	for j in range(len(primes)):
		# if primes[j] > i:
		# 	break
		
		dp[i][j] = (dp[i][j - 1] if j >= 1 else 0) + (dp[i - primes[j]][j] if i >= primes[j] else 0)
		if dp[i][j] >= 5000:
			print("i", i)
			exit(0)
	
	i += 1
