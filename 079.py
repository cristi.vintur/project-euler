ans = ""

def bkt(s, idx, nums):
	global ans

	digs = {}
	for i in range(len(nums)):
		if idx[i] < len(nums[i]):
			d = nums[i][idx[i]]
			if d not in digs:
				digs.update({d : 1})
			else:
				digs[d] = digs[d] + 1

	if not digs:
		if (not ans) or (len(ans) > len(s)):
			ans = s
			print(ans)
		return

	try:
		for d, fr in sorted(digs.items(), key = lambda item : item[1], reverse = True):
			new_idx = idx.copy()
			for i in range(len(nums)):
				if new_idx[i] < len(nums[i]) and nums[i][new_idx[i]] == d:
					new_idx[i] += 1

			bkt(s + d, new_idx, nums)
	except KeyboardInterrupt:
		print(ans)
		exit(0)

nums = []
with open("p079_keylog.txt") as f:
	nums = list(map(lambda s : s[:-1], f.readlines()))

#print(nums)
print(bkt("", [0] * len(nums), nums))

