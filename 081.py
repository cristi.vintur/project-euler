a = []

with open("p081_matrix.txt", "r") as f:
	for line in f.readlines():
		a.append(list(map(int, line.split(','))))

dp = []
for i in range(80):
	dp.append([0] * 80)
	for j in range(80):
		x = 0
		if i == 0 and j == 0:
			x = 0
		elif i == 0:
			x = dp[i][j - 1]
		elif j == 0:
			x = dp[i - 1][j]
		else:
			x = min(dp[i - 1][j], dp[i][j - 1])
		dp[i][j] = a[i][j] + x

print(dp[79][79])
