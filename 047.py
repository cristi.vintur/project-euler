from bisect import bisect_left

VMAX = (10 ** 6) + 10

lpd = list(range(0, VMAX))

for i in range(2, VMAX):
	if lpd[i] != i:
		continue
	
	for j in range(i * i, VMAX, i):
		lpd[j] = min(lpd[j], i)

nr_factors = [0] * VMAX
for i in range(2, VMAX):
	if (i // lpd[i]) % lpd[i] == 0:
		nr_factors[i] = nr_factors[i // lpd[i]]
	else:
		nr_factors[i] = 1 + nr_factors[i // lpd[i]]

f2, f3, f4 = False, False, False
for i in range(2, VMAX):
	if nr_factors[i : i + 2] == [2, 2] and not f2:
		print(2, i)
		f2 = True
	
	if nr_factors[i : i + 3] == [3, 3, 3] and not f3:
		print(3, i)
		f3 = True
	
	if nr_factors[i : i + 4] == [4, 4, 4, 4] and not f4:
		print(4, i)
		f4 = True