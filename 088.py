def bkt(p, s, n, last, ans):
	nr1 = p - s
	if n + nr1 < len(ans):
		ans[n + nr1] = min(ans[n + nr1], p)
	
	while p * last < len(ans):
		bkt(p * last, s + last, n + 1, last, ans)
		last += 1

INF = 10 ** 18
ans = [INF] * 24010

bkt(1, 0, 0, 2, ans)

print(sum(set(ans[2:12001])))
