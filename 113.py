import numpy as np

if __name__ == '__main__':
	
	l = 100

	dp_inc = np.zeros((l + 1, 10), dtype = np.int64)
	for d in range(1, 10):
		dp_inc[1][d] = 1

	for i in range(2, l + 1):
		for d in range(0, 10):
			for prevd in range(0, d + 1):
				dp_inc[i][d] += dp_inc[i - 1][prevd]

	dp_desc = np.zeros((l + 1, 10), dtype = np.int64)
	for d in range(1, 10):
		dp_desc[1][d] = 1

	for i in range(2, l + 1):
		for d in range(0, 10):
			for prevd in range(d, 10):
				dp_desc[i][d] += dp_desc[i - 1][prevd]


	ans = dp_inc.sum() + dp_desc.sum()
	ans -= l * 9
	# there are l numbers with equal digits, for each digit, that are numbered twice
	# 1, 11, 111 ...

	print(ans)
