

L = 10

def is_prime(v):
	d = 2
	while d * d <= v:
		if v % d == 0:
			return False

		d += 1
	
	return True

def generate(v, p, d, l, q):
	for i in range(p + 1):
		for a in range(10):
			if a != d and (a != 0 or i != l - 1):
				q.append((v - d * 10 ** i + a * 10 ** i, i - 1))

def s(l, d):

	if d == 0:
		q = [(i * 10 ** (l - 1), l - 2) for i in range(1, 10)]
	else:
		q = [((10 ** l - 1) // 9 * d, l - 1)]

	nr = l
	while True:
		res = 0
		q2 = []
		for v, p in q:
			#print(v)
			if is_prime(v):
				res += v

			if res == 0:
				generate(v, p, d, l, q2)

		if res > 0:
			#print(d, nr)
			return res

		q = q2
		nr -= 1

ans = 0
for d in range(10):
	ans += s(L, d)

print(ans)

