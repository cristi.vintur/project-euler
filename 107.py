def root(fth, x):
	return x if fth[x] < 0 else root(fth, fth[x])

def join(fth, i, j):
	i, j = root(fth, i), root(fth, j)
	fth[i] = j

with open("data/p107_network.txt", "r") as f:
	edges = []
	i = 0
	for line in f:
		v = list(line.strip().split(","))
		for j in range(len(v)):
			if i < j and v[j] != "-":
				edges.append((int(v[j]), i, j))
		i += 1
	
	n = i
	fth = [-1] * n

	edges.sort()
	ans, cnt = 0, 0
	for w, i, j in edges:
		if root(fth, i) == root(fth, j):
			ans += w
		else:
			join(fth, i, j)
			cnt += 1
	
	print("cnt", cnt)
	print(ans)
