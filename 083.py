import numpy as np
import heapq as pq

INF = 10 ** 9
dl, dc = [-1, 0, 1, 0], [0, 1, 0, -1]

a = []
with open("data/p083_matrix.txt", "r") as f:
#with open("in", "r") as f:
	for line in f:
		a.append(list(map(int, line.split(","))))

N = len(a)

dp = np.full((N, N), INF, dtype = np.int32)
q = []

dp[0][0] = a[0][0]
pq.heappush(q, (a[0][0], 0, 0))

while q:
	val, i, j = pq.heappop(q)
	if val != dp[i][j]:
		continue
	
	for d in range(len(dl)):
		x, y = i + dl[d], j + dc[d]
		if 0 <= x and x < N and 0 <= y and y < N and dp[x][y] > dp[i][j] + a[x][y]:
			dp[x][y] = dp[i][j] + a[x][y]
			pq.heappush(q, (dp[x][y], x, y))

print(dp[N - 1][N - 1])

