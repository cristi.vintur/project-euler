import numpy as np
from util_math import sieve

L = 10 ** 14
V = 4 * (10 ** 6)
P = 2 * (10 ** 7)
MOD = 1234567891011

def mul(a, b, mod):
	c = [[0, 0], [0, 0]]
	for i in range(2):
		for j in range(2):
			c[i][j] = (a[i][0] * b[0][j] + a[i][1] * b[1][j]) % mod
	
	return c

def get_fib(n):
	a = [[0, 1], [1, 1]]
	res = [[1, 0], [0, 1]]
	while n > 0:
		if n & 1:
			res = mul(res, a, MOD)
		a = mul(a, a, MOD)
		n >>= 1
	
	return res[1][0]

_, small_primes = sieve(P)

is_prime = [True] * V
primes = []

for d in small_primes:
	low = L // d * d
	if low < L:
		low += d
	
	for x in range(low, L + V, d):
		is_prime[x - L] = False

primes = [x for x in range(L, L + V) if is_prime[x - L]]
print("primes", len(primes))

ans = 0
for i in range(10 ** 5):
	ans += get_fib(primes[i])
	if ans >= MOD:
		ans -= MOD

print(ans)
