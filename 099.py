from math import log

line_number, bst = -1, 0.0

with open("data/p099_base_exp.txt", "r") as f:
	i = 0
	for line in f:
		i += 1
		base, exp = map(int, line.split(","))
		val = exp * log(base)
		#print(val)
		if val > bst:
			line_number, bst = i, val

print(line_number)
