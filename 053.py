ans = 0
c = [[1], [1, 1]]

for i in range(2, 101):
	c.append([1])
	for j in range(1, i):
		c[-1].append(c[-2][j - 1] + c[-2][j])
		if c[-1][-1] >= 10 ** 6:
			ans += i + 1 - 2 * j
			break
	
	if c[-1][-1] == c[-1][1]:
		c[-1].append(1)

print(ans)