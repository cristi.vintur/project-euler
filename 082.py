import numpy as np
import heapq as pq

N = 80
INF = 10 ** 9
dl, dc = [-1, 0, 1], [0, 1, 0]

a = []
with open("data/p082_matrix.txt", "r") as f:
#with open("in", "r") as f:
	for line in f:
		a.append(list(map(int, line.split(","))))

dp = np.full((N, N), INF, dtype = np.int32)
q = []

for i in range(N):
	dp[i][0] = a[i][0]
	pq.heappush(q, (a[i][0], i, 0))

while q:
	val, i, j = pq.heappop(q)
	if val != dp[i][j]:
		continue
	
	for d in range(len(dl)):
		x, y = i + dl[d], j + dc[d]
		if 0 <= x and x < N and 0 <= y and y < N and dp[x][y] > dp[i][j] + a[x][y]:
			dp[x][y] = dp[i][j] + a[x][y]
			pq.heappush(q, (dp[x][y], x, y))

ans = INF
for i in range(N):
	ans = min(ans, dp[i][N - 1])

print(ans)

