from itertools import combinations

V = 55

def check1(v):
	l, r = v[0], 0
	for i in range(1, (len(v) + 1) // 2):
		l += v[i]
		r += v[len(v) - i]
		if l <= r:
			return False
	return True

def check2(v):
	nums = list(range(len(v)))
	for nr in range(1, len(v)):
		for lhs in combinations(nums, nr):
			rem = list(filter(lambda x : x not in lhs, nums))
			for rhs in combinations(rem, nr):
				sl, sr = 0, 0
				for i in lhs:
					sl += v[i]
				for j in rhs:
					sr += v[j]
				if sl == sr:
					#print(v, lhs, rhs, sl, sr)
					return False
	return True

def bkt(v, n):
	if len(v) == n:
		print(sum(v), "".join([str(x) for x in v]))
		#exit(0)
		return
	
	x = (1 if len(v) == 0 else v[-1] + 1)
	while x < V:
		v.append(x)
		#print(v)
		if not check1(v):
			v.pop()
			break
		if check2(v):
			bkt(v, n)
		
		v.pop()
		x += 1

n = int(input())

bkt([], n)

