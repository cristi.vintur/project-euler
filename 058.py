def is_prime(n):
	d = 2
	while d * d <= n:
		if n % d == 0:
			return False
		d += 1
	
	return True

nr, nrTotal = 0, 1
lg = 1

while True:
	lg += 2
	nrTotal += 4
	
	for i in range(1, 4):
		if is_prime(lg * lg - i * (lg - 1)):
			nr += 1
	
	if nr * 10 < nrTotal:
		print(nr)
		print(nrTotal)
		print(lg)
		exit(0)