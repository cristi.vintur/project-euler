def isPandigital(x):
	fr = [0] * 9
	while x:
		if x % 10 == 0:
			return False
		if fr[x % 10 - 1] > 0:
			return False
		fr[x % 10 - 1] = 1
		x //= 10
	return True

def ok(n):
	if n < (10 ** 9):
		return False
	
	if not isPandigital(n % (10 ** 9)):
		return False
	
	pref = ''.join(sorted(str(n)[:9]))
	return pref == "123456789"

a, b, i = 1, 1, 2
while True:
	c = a + b
	i += 1
	if ok(c):
		print(i)
		exit(0)

	if i % 10000 == 0:
		print(i)
	a, b = b, c
