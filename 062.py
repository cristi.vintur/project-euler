VMAX = 10000

fr = [0] * VMAX
d = {}
for i in range(1, VMAX):
	s = str(i * i * i)
	s = ''.join(sorted(list(s)))
	
	if s not in d:
		d[s] = i
	
	fr[d[s]] += 1

for i in range(VMAX):
	if fr[i] == 5:
		print(i * i * i)
		exit(0)