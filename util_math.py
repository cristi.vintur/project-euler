from math import sqrt, gcd

def crt(vr, vm):
	# solves the equations x = r[i] (mod m[i]) assuming m[i] are pairwise coprime
	if (not vr) or len(vr) != len(vm):
		raise ValueError("Invalid arguments r = {}, m = {}".format(vr, vm))
	
	prodm = 1
	for m in vm:
		prodm *= m
	
	x = 0
	for r, m in zip(vr, vm):
		mm = prodm // m
		c = (r * pow(mm, m - 2, m)) % m
		x += c * mm
	
	return x % prodm

def prime_exp_in_factorial(p, n):
	ans = 0
	while n >= p:
		ans += n // p
		n //= p
	
	return ans

def sieve(n):
	is_prime = [True] * n
	primes = []

	is_prime[0], is_prime[1] = False, False
	for i in range(2, n):
		if not is_prime[i]:
			continue
		
		primes.append(i)
		for j in range(i * i, n, i):
			is_prime[j] = False
	
	return is_prime, primes

def is_prime(n):
	if n <= 1:
		return False

	d = 2
	while d * d <= n:
		if n  % d == 0:
			return False
		d += 1
	return True

# returns a vector [0..n) where lpd[i] = lowest prime divisor of n
def get_lpd(n):
	lpd = list(range(n))

	for i in range(2, n):
		if lpd[i] == i:
			for j in range(i, n, i):
				lpd[j] = min(lpd[j], i)
	
	return lpd

# returns a vector [0..n) where divSum[i] = sum of all divisors of number n
# lpd is a vector which satisfies the following condition: lpd[i] is a prime divisor of i
def get_divisor_sum(n, lpd):
	divSum = [0] * n
	divSum[1] = 1
	for i in range(2, n):
		p, pw, ii = lpd[i], 1, i
		while ii % p == 0:
			ii //= p
			pw *= p

		divSum[i] = divSum[ii] * (pw * p - 1) // (p - 1)
	
	return divSum

def get_phi(n):
	phi = list(range(0, n))
	
	for i in range(2, n):
		if phi[i] != i:
			continue
		
		for j in range(i, n, i):
			phi[j] = phi[j] // i * (i - 1)
	
	return phi

def get_continued_fraction(d):
	def val(t, sqrtd):
		return (t[0] + t[1] * sqrtd) / t[2]
	
	sqrtd = sqrt(d)
	a, A = [], []
	
	A.append((0, 1, 1))
	a.append(int(val(A[-1], sqrtd)))
	
	while True:
		t = A[-1]
		aux = t[0] - a[-1] * t[2]
		x, y, z = t[2] * aux, -t[2] * t[1], aux ** 2 - d * t[1] ** 2
		g = gcd(x, gcd(y, z))
		A.append((x // g, y // g, z // g))
		a.append(int(val(A[-1], sqrtd)))
		
		if y == z and x - a[-1] * z == -z * int(sqrtd):
			return a
