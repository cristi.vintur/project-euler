from util_math import sieve
from heapq import heappush, heappop

nr_sol = int(input())

def push(q, n, p, l):
	if (p + 1) // 2 > nr_sol:
		print(n)
		exit(0)
	
	heappush(q, (n, p, l))

is_prime, primes = sieve(200)

q = []
heappush(q, (1, 1, []))

while True:
	n, p, l = heappop(q)
	for i in range(len(l)):
		if i == 0 or l[i - 1] > l[i]:
			newp = p // (2 * l[i] + 1) * (2 * l[i] + 3)
			newl = l.copy()
			newl[i] += 1
			push(q, n * primes[i], newp, newl)
	
	newp = p * 3
	newl = l.copy()
	newl.append(1)
	push(q, n * primes[len(l)], newp, newl)
