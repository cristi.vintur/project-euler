N = 100

win = [[False] * N for _ in range(N)]

for i in range(N):
	for j in range(1, i + 1):
		for take in range(1, j + 1):
			if not win[i - take][min(i - take, 2 * take)]:
				win[i][j] = True
				break

for i in range(N):
	for j in range(1, i + 1):
		if win[i][j]:
			print(i, j)
			break

V = 23416728348467685
#V = 13

fib = [1, 2]
while fib[-1] < V:
	fib.append(fib[-2] + fib[-1])

s = [1, 3]
for i in range(2, len(fib)):
	s.append(s[i - 1] + s[i - 2] - fib[i - 2] + fib[i])

print(s[-1])

# S_n = S_{n-1} + S_{n-2} - F_{n-2}
