def read_file(filename):
	with open(filename) as f:
		v = []
		for line in f:
			v.append(list(map(int, line.split())))
	
		return v
	return None

v = read_file('p067_triangle.txt')
for i in range(1, len(v)):
	for j in range(i):
		if j == 0:
			v[i][j] += v[i - 1][j]
		else:
			v[i][j] += max(v[i - 1][j - 1], v[i - 1][j])

print(max(v[-1]))
