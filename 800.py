from math import log

def sieve(n):
	is_prime = [True] * n
	primes = []

	is_prime[0], is_prime[1] = False, False
	for i in range(2, n):
		if not is_prime[i]:
			continue
		
		primes.append(i)
		for j in range(i * i, n, i):
			is_prime[j] = False
	
	return is_prime, primes

def upper_bound(v, p, a, b):
	l, r = 0, len(v)
	while l < r:
		mid = (l + r) // 2
		if v[mid] * log(p) + p * log(v[mid]) <= b * log(a):
			l = mid + 1
		else:
			r = mid

	return l

a, b = map(int, input().split())
# calculate C(a ^ b)

is_prime, primes = sieve(int(log(a) / log(2) * a))

ans = 0
for i in range(len(primes)):
	if primes[i] >= a:
		break

	j = upper_bound(primes, primes[i], a, b)
	if j > i:
		ans += j - i - 1

print(ans)

