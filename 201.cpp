#include <bits/stdc++.h>
using namespace std;
using uint = unsigned int;
using ll = long long;
using ld = long double;
using ull = unsigned long long;
using pii = pair<int, int>;
using pll = pair<ll, ll>;
#define dbg(x) cerr<<#x": "<<(x)<<endl
#define dbg_p(x) cerr<<#x": "<<(x).first<<' '<<(x).second<<endl
#define dbg_v(x, n) {cerr<<#x"[]: ";for(long long _=0;_<n;++_)cerr<<(x)[_]<<' ';cerr<<endl;}
#define all(v) v.begin(), v.end()
#define fi first
#define se second

template<typename T1, typename T2>
ostream& operator <<(ostream &out, const pair<T1, T2> &item) {
	out << '(' << item.first << ", " << item.second << ')';
	return out;
}

template <typename T>
ostream& operator <<(ostream &out, const vector<T>& v) {
	for(const auto &item : v) out << item << ' ';
	return out;
}

const int N = 101;
const int K = 51;
const int S = 350000;

// subsets of size <K, with numbers <N

char dp[K][S];

int main()
{
	ios_base::sync_with_stdio(false);
	
	dp[0][0] = 1;
	for(int i = 1; i < N; ++i)
		for(int j = K - 1; j >= 1; --j)
			for(int s = S - 1; s >= i * i; --s) {
				dp[j][s] += dp[j - 1][s - i * i];
				if(dp[j][s] > 2) dp[j][s] = 2;
			}

	ll ans = 0;
	for(int s = 0; s < S; ++s) if(dp[K - 1][s] == 1) ans += s;

	cout << ans << '\n';

	return 0;
}
