def compute(n_dice, max_value):
	v = [0] * (n_dice * max_value + 1)
	v[0] = 1

	for i in range(n_dice):
		for s in range(len(v) - 1, -1, -1):
			v[s] = 0
			for last in range(1, min(max_value, s) + 1):
				v[s] += v[s - last] / max_value

	return v

if __name__ == '__main__':
	peter = compute(9, 4)
	colin = compute(6, 6)

	ans = 0
	for sum_peter in range(len(peter)):
		for sum_colin in range(min(len(colin), sum_peter)):
			ans += peter[sum_peter] * colin[sum_colin]

	print(ans)
