from fractions import Fraction
import numpy as np

if __name__ == '__main__':
	n = 15

	# dp[i][j] = probability after i rounds to have j blue discs
	dp = np.zeros((n + 1, n + 1), dtype = Fraction)

	dp[0][0] = Fraction(1, 1)
	for i in range(1, n + 1):
		for j in range(0, i + 1):
			dp[i][j] = dp[i - 1][j] * Fraction(i, i + 1)
			if j > 0:
				dp[i][j] += dp[i - 1][j - 1] * Fraction(1, i + 1)

	p = 0
	for j in range(n // 2 + 1, n + 1):
		p += dp[n][j]

	x, y = p.numerator, p.denominator
	print(y // x)
