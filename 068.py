from itertools import permutations

nums = list(range(1, 11))

ans = 0
for outer in permutations(nums, 5):
	if outer[0] != min(outer) or max(outer) != 10:
		continue
	
	# 5 * s_line = 2 * s_inner + s_outer = 2 * (1 + ... + 10) - s_outer
	aux = 2 * sum(nums) - sum(outer)
	if aux % 5 != 0:
		continue
	
	s_line = aux // 5
	
	# print(outer, s_line)
	for x in nums:
		if x not in outer:
			inner = [x]
			ok = True
			for i in range(len(outer) - 1):
				y = s_line - outer[i] - inner[i]
				if (y in inner) or (y in outer) or (y not in nums):
					ok = False
				inner.append(y)
			
			if not ok:
				continue
			
			s = ""
			for i in range(len(outer)):
				s += str(outer[i])
				s += str(inner[i])
				s += str(inner[(i + 1) % len(inner)])
			
			if int(s) > ans:
				ans = int(s)

print(ans)