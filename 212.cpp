#include <bits/stdc++.h>
using namespace std;
using uint = unsigned int;
using ll = long long;
using ld = long double;
using ull = unsigned long long;
using pii = pair<int, int>;
using pll = pair<ll, ll>;
#define dbg(x) cerr<<#x": "<<(x)<<endl
#define dbg_p(x) cerr<<#x": "<<(x).first<<' '<<(x).second<<endl
#define dbg_v(x, n) {cerr<<#x"[]: ";for(long long _=0;_<n;++_)cerr<<(x)[_]<<' ';cerr<<endl;}
#define all(v) v.begin(), v.end()
#define fi first
#define se second

template<typename T1, typename T2>
ostream& operator <<(ostream &out, const pair<T1, T2> &item) {
	out << '(' << item.first << ", " << item.second << ')';
	return out;
}

template <typename T>
ostream& operator <<(ostream &out, const vector<T>& v) {
	for(const auto &item : v) out << item << ' ';
	return out;
}

const int V = 10500;
const int SMOD = 1000000;

struct YNode {
	int whole, cnt;
	YNode *nl, *nr;

	YNode() { whole = cnt = 0; nl = nr = nullptr; }

	int get(YNode *node) { return node ? node->cnt : 0; }

	int compute(int l, int r) { return whole > 0 ? r - l + 1 : get(nl) + get(nr); }

	void update(int l, int r, int a, int b, int val) {
		if(a <= l && r <= b) { whole += val; cnt = compute(l, r); return; }

		int mid = (l + r) / 2;
		if(a <= mid) { if(!nl) nl = new YNode(); nl->update(l, mid, a, b, val); }
		if(mid + 1 <= b) { if(!nr) nr = new YNode(); nr->update(mid + 1, r, a, b, val); }

		cnt = compute(l, r);
	}

	int query(int l, int r, int a, int b) {
		if(a <= l && r <= b) return cnt;

		int mid = (l + r) / 2;
		int vl = (a <= mid && nl ? nl->query(l, mid, a, b) : 0);
		int vr = (mid + 1 <= b && nr ? nr->query(mid + 1, r, a, b) : 0);

		return vl + vr;
	}
};

struct Lol {
	int x1, x2, y1, y2, val;
};

YNode *st[V];
vector<Lol> events[V];

int calc(int k) {
	int res = (100003 - 200003LL * k) % SMOD + SMOD;
	int aux = (300007LL * k % SMOD) * (1LL * k * k % SMOD) % SMOD;
	return (res + aux) % SMOD;
}

int main()
{
	ios_base::sync_with_stdio(false);
	
	vector<int> s = { 0 };
	for(int i = 1; i <= 55; ++i) {
		s.push_back(calc(i));
	}
	for(int i = 56; i <= 300000; ++i)
		s.push_back((s[i - 24] + s[i - 55]) % SMOD);

	for(int i = 1; i <= 50000; ++i) {
		int x = s[6 * i - 5] % 10000;
		int y = s[6 * i - 4] % 10000;
		int z = s[6 * i - 3] % 10000;
		int dx = 1 + s[6 * i - 2] % 399;
		int dy = 1 + s[6 * i - 1] % 399;
		int dz = 1 + s[6 * i - 0] % 399;

		events[z].push_back({x, x + dx - 1, y, y + dy - 1, 1});
		events[z + dz].push_back({x, x + dx - 1, y, y + dy - 1, -1});
	}

	ll curr = 0, ans = 0;

	for(int i = 0; i < V; ++i) st[i] = new YNode();

	for(int i = 0; i < V; ++i) {
		if(i % 1000 == 0) dbg(i);
		for(const auto &e : events[i])
			for(int x = e.x1; x <= e.x2; ++x) {
				curr -= st[x]->cnt;
				st[x]->update(0, V - 1, e.y1, e.y2, e.val);
				curr += st[x]->cnt;
			}

		ans += curr;
	}
	
	cout << ans << '\n';

	return 0;
}
