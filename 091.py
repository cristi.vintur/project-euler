V = 50

ans = 0
for x1 in range(V + 1):
	for y1 in range(V + 1):
		for x2 in range(V + 1):
			for y2 in range(V + 1):
				if (x1, y1) == (x2, y2) or (x1, y1) == (0, 0) or (x2, y2) == (0, 0):
					continue
				a, b, c = x1 * x1 + y1 * y1, x2 * x2 + y2 * y2, (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)
				if a + b == c or b + c == a or c + a == b:
					ans += 1

print(ans // 2)
