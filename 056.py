ans = 0

for a in range(2, 101):
	for b in range(2, 101):
		s = sum([ord(x) - ord('0') for x in str(pow(a, b))])
		ans = max(ans, s)

print(ans)