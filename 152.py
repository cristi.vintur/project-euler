from fractions import Fraction

n = int(input())

nums = list(range(2, n + 1))
s = [0] * (1 + len(nums))
s[-1] = Fraction(0)
for i in range(len(nums) - 1, -1, -1):
	s[i] = s[i + 1] + Fraction(1, nums[i] ** 2)

print(s[0])
ans = 0

def bkt(v, i, vec):
	global ans, nums, s

	if v == 0:
		ans += 1
		return
	
	if i == len(nums) or v > s[i]:
		return
	
	print(vec)
	
	while i < len(nums):
		if v >= Fraction(1, nums[i] ** 2):
			vec.append(nums[i])
			bkt(v - Fraction(1, nums[i] ** 2), i + 1, vec)
			vec.pop()
		i += 1

bkt(Fraction(1, 2), 0, [])
print(ans)
