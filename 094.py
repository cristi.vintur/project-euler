from math import sqrt

def is_square(x):
	s = int(sqrt(x))
	return s * s == x

P = 10 ** 9
#P = 16

ans = 0
nr_triangles = 0

# count triangles with sides (a, a, a + 1), a = 2k+1
# k should be perfect square or double of perfect square

x, k = 1, 1
while 3 * (2 * k + 1) + 1 <= P:
	# k = x ** 2
	if is_square(3 * k + 2):
		ans += 3 * (2 * k + 1) + 1
		nr_triangles += 1
	
	if 3 * (4 * k + 1) + 1 <= P and is_square(3 * k + 1):
		ans += 3 * (4 * k + 1) + 1
		nr_triangles += 1
	
	x += 1
	k = x ** 2

# count triangles with sides (a - 1, a, a), a = 2k-1
# k should be perfect square or double of perfect square

x, k = 2, 4
while 3 * (2 * k - 1) - 1 <= P:
	# k = x ** 2
	if is_square(3 * k - 2):
		ans += 3 * (2 * k - 1) - 1
		nr_triangles += 1
	
	if 3 * (4 * k - 1) - 1 <= P and is_square(3 * k - 1):
		ans += 3 * (4 * k - 1) - 1
		nr_triangles += 1
	
	x += 1
	k = x ** 2

print(ans)
print("nr_triangles", nr_triangles)
