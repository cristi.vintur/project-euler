def get_poly_value(vx, vy, x0):
	res = 0
	for i in range(len(vx)):
		up, down = vy[i], 1
		for j in range(len(vx)):
			if i == j:
				continue
			up *= x0 - vx[j]
			down *= vx[i] - vx[j]
		res += up // down
	return res

def eval(coefs, x):
	res = 0
	for coef in reversed(coefs):
		res = res * x + coef
	return res

ans = 0
coefs = [1, -1, 1, -1, 1, -1, 1, -1, 1, -1, 1]
for i in range(1, len(coefs)):
	vx = list(range(1, i + 1))
	vy = [eval(coefs, x) for x in vx]
	ans += get_poly_value(vx, vy, i + 1)

print(ans)
