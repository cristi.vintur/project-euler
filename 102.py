from geometry import area

def read_file(filename):
	with open(filename) as f:
		v = []
		
		for line in f:
			v.append(list(map(int, line.split(','))))
		
		return v
	
	return None

def check(a, b, c):
	o = [0, 0]
	return area(a, b, o) + area(b, c, o) + area(c, a, o) == area(a, b, c)

v = read_file('p102_triangles.txt')

ans = 0
for tri in v:
	ans += check(tri[0:2], tri[2:4], tri[4:6])

print(ans)