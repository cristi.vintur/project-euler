def is_palidrom(n):
	return str(n) == ''.join(reversed(str(n)))

def lychrel(n):
	for _ in range(50):
		n += int(''.join(reversed(str(n))))
		if is_palidrom(n):
			return False
	
	return True

ans = 0
for i in range(1, 10001):
	if lychrel(i):
		ans += 1

print(ans)