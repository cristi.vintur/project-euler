N = 101

p = 0.02

dp = [[0, 0] for _ in range(N)]
dp[1] = [1, 0]
for i in range(2, N):
	if i % 1000 == 0:
		print(i)
	
	dp[i] = [i, i]
	j0, j1 = 1, 1
	a, b = 1, 1 - (1 - p) ** i

	for j in range(1, i):
		a *= 1 - p
		prob_not_infected0 = (1 - p) ** j
		prob_not_infected1 = 1 - (1 - a) / b
		x0 = 1 + prob_not_infected0 * dp[i - j][0] + (1 - prob_not_infected0) * (dp[j][1] + dp[i - j][0])
		x1 = 1 + prob_not_infected1 * dp[i - j][1] + (1 - prob_not_infected1) * (dp[j][1] + dp[i - j][0])
		
		if x0 < dp[i][0]:
			dp[i][0], j0 = x0, j
		if x1 < dp[i][1]:
			dp[i][1], j1 = x1, j
	
	prob_not_infected = (1 - p) ** i
	x = 1 + (1 - prob_not_infected) * dp[i][1]
	if x < dp[i][0]:
		dp[i][0], j0 = x, -1
	
	print(i, j0, j1)

print(dp[N - 1])
exit(0)

def nr_tests(cfg, n):
	if cfg == 0:
		return 1
	
	res = n + 1
	for i in range(n - 1, -1, -1):
		if (cfg & (1 << i)) == 0:
			res -= 1
		else:
			break
	
	return res

n = 2
avg = 0
for cfg in range(2 ** n):
	prob = 1.0
	for i in range(n):
		prob *= (p if (cfg & (1 << i)) > 0 else 1 - p)
	avg += prob * nr_tests(cfg, n)

print(avg)
