def sum_digits(n):
	s = 0
	while n > 0:
		s += n % 10
		n //= 10
	return s

if __name__ == '__main__':
	
	k = 30
	n = 80

	numbers = []

	i = 2
	while n > 0:
		pi = i
		while 9 * pi <= (10 ** i) - 1:
			if pi >= 10 and sum_digits(pi) == i:
				numbers.append((i, pi))
				n -= 1

			pi *= i

		i += 1

	numbers.sort(key = lambda x : x[1])
	# print(numbers)
	print(numbers[k - 1][1])
