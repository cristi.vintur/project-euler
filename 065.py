v = [2]

x = 2
while len(v) <= 100:
	v += [1, x, 1]
	x += 2

x, y = v[99], 1
for i in range(98, -1, -1):
	x, y = y, x
	x += v[i] * y

print(sum(map(int, str(x))))
