from itertools import combinations

def check1(v):
	l, r = v[0], 0
	for i in range(1, (len(v) + 1) // 2):
		l += v[i]
		r += v[len(v) - i]
		if l <= r:
			return False
	return True

def check2(v):
	nums = list(range(len(v)))
	for nr in range(1, len(v)):
		for lhs in combinations(nums, nr):
			rem = list(filter(lambda x : x not in lhs, nums))
			for rhs in combinations(rem, nr):
				sl, sr = 0, 0
				for i in lhs:
					sl += v[i]
				for j in rhs:
					sr += v[j]
				if sl == sr:
					#print(v, lhs, rhs, sl, sr)
					return False
	return True

with open("data/p105_sets.txt", "r") as f:
	ans = 0
	for line in f:
		v = list(map(int, line.split(",")))
		v.sort()
		if check1(v) and check2(v):
			ans += sum(v)
	
	print(ans)

