HIGH_CARD = 0
ONE_PAIR = 1
TWO_PAIRS = 2
THREE_OF_A_KIND = 3
STRAIGHT = 4
FLUSH = 5
FULL_HOUSE = 6
FOUR_OF_A_KIND = 7
STRAIGHT_FLUSH = 8
ROYAL_FLUSH = 9

def get_value(c):
	if c == 'T':
		return 10
	if c == 'J':
		return 11
	if c == 'Q':
		return 12
	if c == 'K':
		return 13
	if c == 'A':
		return 14
	return ord(c) - ord('0')

def get_card(s):
	return (get_value(s[0]), s[1])

def values_in_order(cards):
	for i in range(1, len(cards)):
		if cards[i - 1][0] + 1 != cards[i][0]:
			return False
	return True

def same_suits(cards):
	for i in range(1, len(cards)):
		if cards[i - 1][1] != cards[i][1]:
			return False
	return True

def same_values(cards):
	for i in range(1, len(cards)):
		if cards[i - 1][0] != cards[i][0]:
			return False
	return True

def get_pairs(cards):
	res = []
	for i in range(1, len(cards)):
		if cards[i - 1][0] == cards[i][0]:
			res.append(cards[i][0])
	return res

def get_hand(cards):
	cards.sort(key = lambda x : x[0])

	straight = values_in_order(cards)
	flush = same_suits(cards)

	if straight and flush:
		if cards[0][0] == 10:
			return [ROYAL_FLUSH]
		else:
			return [STRAIGHT_FLUSH, cards[-1][0]]
	
	if same_values(cards[:4]) or same_values(cards[1:]):
		return [FOUR_OF_A_KIND, cards[2][0]]
	
	if (same_values(cards[:3]) and same_values(cards[3:])) or (same_values(cards[:2]) and same_values(cards[2:])):
		return [FULL_HOUSE, cards[2][0]]
	
	if flush:
		return [FLUSH, cards[4][0]]
	
	if straight:
		return [STRAIGHT, cards[4][0]]

	if same_values(cards[:3]) or same_values(cards[1:4]) or same_values(cards[2:]):
		return [THREE_OF_A_KIND, cards[2][0]]
	
	pairs = get_pairs(cards)
	if len(pairs) == 2:
		return [TWO_PAIRS, pairs[1], pairs[0]]
	
	if len(pairs) == 1:
		return [ONE_PAIR, pairs[0]]

	cards_values = list(map(lambda x : x[0], reversed(cards)))
	return [HIGH_CARD] + cards_values

with open("data/p054_poker.txt", "r") as in_file:
	p1_win = 0

	for line in in_file:
		cards = list(map(get_card, line.split()))
		p1 = cards[:5]
		p2 = cards[5:]

		a1 = get_hand(p1)
		a2 = get_hand(p2)

		if a1 > a2:
			p1_win += 1
	
	print(p1_win)

