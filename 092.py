VMAX = 10 ** 7 + 1

def f(x):
	return sum([int(d) ** 2 for d in str(x)])

def get(x, end):
	if end[x]:
		return end[x]
	
	x = f(x)
	while x >= VMAX:
		x = f(x)
	
	return get(x, end)

end = [0] * VMAX
end[1] = 1
end[89] = 89

for i in range(2, VMAX):
	if i % 100000 == 0:
		print(i)
	
	end[i] = get(i, end)

print(end.count(89))