from math import gcd

def sieve(n):
	is_prime = [True] * n
	primes = []

	is_prime[0], is_prime[1] = False, False
	for i in range(2, n):
		if not is_prime[i]:
			continue
		
		primes.append(i)
		for j in range(i * i, n, i):
			is_prime[j] = False
	
	return is_prime, primes

MOD = 10 ** 9 + 7

k = int(input())

#f = 1
#for i in range(2, k + 1):
#	f *= 2 * i // gcd(f, 2 * i)

#print(f % MOD)


is_prime, primes = sieve(k + 1)

print("Sieve done")

ans = 2
for p in primes:
	pw = p
	while pw <= k:
		ans = (ans * p) % MOD
		pw *= p

print(ans)

