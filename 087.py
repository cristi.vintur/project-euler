from math import sqrt
from util_math import sieve

V = 50 * (10 ** 6)
#V = 50

is_prime, primes = sieve(int(sqrt(V)) + 69)

can = [False] * V

for p1 in primes:
	for p2 in primes:
		s1 = (p1 ** 2) + (p2 ** 3)
		if s1 >= V:
			break

		for p3 in primes:
			s2 = s1 + (p3 ** 4)
			if s2 >= V:
				break

			can[s2] = True

print(sum(can))
