def prime_exp_in_factorial(p, n):
	ans = 0
	while n >= p:
		ans += n // p
		n //= p
	
	return ans

def sieve(n):
	is_prime = [True] * n
	primes = []
	
	for i in range(2, n):
		if not is_prime[i]:
			continue
		
		primes.append(i)
		for j in range(i * i, n, i):
			is_prime[j] = False
	
	return is_prime, primes

def f(n, primes):
	ans = 0
	for p in primes:
		if p > n:
			return ans
		
		ans += prime_exp_in_factorial(p, n) * p
	
	return ans

n = 20000000
k = 15000000

is_prime, primes = sieve(n + 1)

ans = f(n, primes) - f(k, primes) - f(n - k, primes)

print(ans)
