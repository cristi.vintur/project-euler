def ok(x, y):
	return str(x)[2:] == str(y)[:2]

def bkt(v, used, nums):
	if len(v) == len(used):
		if ok(v[-1], v[0]):
			return sum(v)
		return None
	
	for i in range(len(used)):
		if used[i]:
			continue
		
		for x in nums[i]:
			if ok(v[-1], x):
				v.append(x)
				used[i] = True
				
				ret = bkt(v, used, nums)
				if ret != None:
					return ret
				
				used[i] = False
				v.pop()
	
	return None

formulas = [
	lambda n : n * (n + 1) // 2,
	lambda n : n * n,
	lambda n : n * (3 * n - 1) // 2,
	lambda n : n * (2 * n - 1),
	lambda n : n * (5 * n - 3) // 2,
	lambda n : n * (3 * n - 2),
]

nums = []
for formula in formulas:
	nums.append([])
	nums[-1] = [formula(x) for x in range(0, 200) if (1000 <= formula(x) and formula(x) < 10000)]

used = [False] * len(nums)

for x in nums[-1]:
	v = [x]
	used[-1] = True
	
	ret = bkt(v, used, nums)
	if ret != None:
		print(ret)
		exit(0)