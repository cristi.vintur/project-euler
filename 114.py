import numpy as np

def get(v, i):
	if i < 0 or i >= len(v):
		return 0

	return v[i]

if __name__ == '__main__':
	
	n = 50

	dp = np.zeros(n + 1, dtype = np.int64)
	dp[0] = 1

	s = 1
	for i in range(1, n + 1):
		dp[i] = s - get(dp, i - 2) - get(dp, i - 3)
		if i >= 3:
			dp[i] += 1

		s += dp[i]

	print(dp[n])
