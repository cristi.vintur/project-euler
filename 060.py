import itertools

VMAX = 10000

def check_prime(n):
	global primes
	
	for p in primes:
		if p * p > n:
			return True
		if n % p == 0:
			return False
	return True

def sieve(n):
	is_prime = [True] * n
	primes = []
	
	for i in range(2, n):
		if not is_prime[i]:
			continue
		
		primes.append(i)
		for j in range(i * i, n, i):
			is_prime[j] = False
	
	return is_prime, primes

def ok(x):
	global is_prime
	
	for y in v:
		val = int(str(x) + str(y))
		if val >= VMAX:
			if not check_prime(val):
				return False
		elif not is_prime[val]:
			return False
		
		val = int(str(y) + str(x))
		if val >= VMAX:
			if not check_prime(val):
				return False
		elif not is_prime[val]:
			return False
	
	return True

def bkt(p):
	global ans, primes
	
	if len(v) == 5:
		s = sum(v)
		if s < ans:
			ans = s
			print(ans)
		return
	
	for i in range(p + 1, len(primes)):
		if ok(primes[i]):
			v.append(primes[i])
			bkt(i)
			v.pop()

ans = 10 ** 18
v = []
is_prime, primes = sieve(VMAX)

bkt(0)
print(ans)