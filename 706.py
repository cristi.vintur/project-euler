import numpy as np

MOD = 10 ** 9 + 7

n = 10 ** 5

dp_old = np.zeros((3, 3, 3, 3), dtype = np.int)
dp_new = dp_old.copy()
dp_new[1][0][0][0] = 1

for i in range(n):
	if i % 10000 == 0:
		print(i)
	
	dp_old = dp_new.copy()
	dp_new.fill(0)
	for x in range(3):
		for y in range(3):
			for z in range(3):
				for s in range(3):
					for d in range(3):
						nr = 3 + (1 if d == 0 and i > 0 else 0)
						xx, yy, zz = x, y, z
						r = (s + d) % 3
						if r == 0:
							xx = (xx + 1) % 3
						elif r == 1:
							yy = (yy + 1) % 3
						else:
							zz = (zz + 1) % 3

						dp_new[xx][yy][zz][(s + d) % 3] = (dp_new[xx][yy][zz][(s + d) % 3] + dp_old[x][y][z][s] * nr) % MOD

ans = 0
for x in range(3):
	for y in range(3):
		for z in range(3):
			for s in range(3):
				r = 0
				if x == 2:
					r += 1
				if y == 2:
					r += 1
				if z == 2:
					r += 1

				if r == 0 or r == 3:
					ans = (ans + dp_new[x][y][z][s]) % MOD

print(ans)
