def get(i, dp, pre, fact, h = 0):
	# NOT cheating :)
	if h > 100:
		dp[i] = 100
		return dp[i]
	
	if dp[i] != 0:
		return dp[i]
	if i in pre:
		return pre[i]
	
	nxt = sum([fact[ord(ch) - ord('0')] for ch in str(i)])
	dp[i] = 1 + get(nxt, dp, pre, fact, h + 1)
	
	return dp[i]

VMAX = 3265920 + 69

fact = [1]
for i in range(1, 10):
	fact.append(fact[-1] * i)

pre = {}
pre[1] = 1
pre[169] = 3
pre[363601] = 3
pre[1454] = 3
pre[871] = 2
pre[45361] = 2
pre[872] = 2
pre[45362] = 2

dp = [0] * VMAX
for i in range(1, VMAX):
	dp[i] = get(i, dp, pre, fact)

ans = 0
for i in range(1, 10 ** 6):
	if dp[i] == 60:
		ans += 1

print(ans)
