def make_vector(a, last, last_used, n):
	return a + [last, last_used] + n

def update(cnt, v, value):
	key = str(v)
	if key not in cnt:
		cnt[key] = 0
	
	cnt[key] += value

def process(cnt, a, last, last_used, n, value):
	#print(a, last, last_used, n, value)
	if last != 4:
		# put the last card again
		aa, nn = a.copy(), n.copy()
		aa[last] -= 1
		aa[last + 1] += 1
		if last_used:
			nn[last - 2] -= 1
		nn[last - 1] += 1
		update(cnt, make_vector(aa[:-1], last + 1, 1, nn), value)
	
	for i in range(0, 4):
		if a[i] == 0:
			continue

		aa, nn = a.copy(), n.copy()
		aa[i] -= 1
		aa[i + 1] += 1

		if i == last:
			c0, c1 = 0, 0
			if last_used:
				c0, c1 = a[i] - n[i - 2], n[i - 2] - 1
			else:
				used = n[i - 2] if i >= 2 else 0
				c0, c1 = a[i] - 1 - used, used
			
			if c0 > 0:
				update(cnt, make_vector(aa[:-1], i + 1, 0, nn), value * c0)

			if c1 > 0:
				nn[i - 2] -= 1
				nn[i - 1] += 1
				update(cnt, make_vector(aa[:-1], i + 1, 1, nn), value * c1)
		else:
			used = n[i - 2] if i >= 2 else 0
			c0, c1 = a[i] - used, used

			if c0 > 0:
				update(cnt, make_vector(aa[:-1], i + 1, 0, nn), value * c0)

			if c1 > 0:
				nn[i - 2] -= 1
				nn[i - 1] += 1
				update(cnt, make_vector(aa[:-1], i + 1, 1, nn), value * c1)

cnt = {}

update(cnt, make_vector([12, 1, 0, 0], 1, 0, [0, 0, 0]), 13)
#update(cnt, make_vector([1, 1, 0, 0], 1, 0, [0, 0, 0]), 2)

for a0 in range(13, -1, -1):
	for a1 in range(13 - a0, -1, -1):
		for a2 in range(13 - a0 - a1, -1, -1):
			for a3 in range(13 - a0 - a1 - a2, -1, -1):
				a4 = 13 - a0 - a1 - a2 - a3
				a = [a0, a1, a2, a3, a4]
				for last in range(1, 5):
					for last_used in range(0, 2):
						if a[last] == 0 or (last_used and last <= 1):
							continue

						for n2 in range(0, a2 + 1):
							for n3 in range(0, a3 + 1):
								for n4 in range(0, a4 + 1):
									n = [n2, n3, n4]
									if last_used and n[last - 2] == 0:
										continue

									key = str(make_vector(a[:-1], last, last_used, n))
									if key not in cnt:
										continue

									process(cnt, a, last, last_used, n, cnt[key])

all_pos, nr = 0, 0
for key, value in cnt.items():
	v = list(map(int, key[1:-1].split(", ")))
	#print(v[:4], v[4], v[5], v[6:], value)
	if sum(v[:4]) == 0:
		all_pos += value
		x = 13 - sum(v[-3:])
		if x in [2, 3, 5, 7, 11, 13]:
			nr += value

print(nr / all_pos)
