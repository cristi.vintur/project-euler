def value(ch):
	if ch == 'I':
		return 1
	if ch == 'V':
		return 5
	if ch == 'X':
		return 10
	if ch == 'L':
		return 50
	if ch == 'C':
		return 100
	if ch == 'D':
		return 500
	return 1000

def to_decimal(line):
	res, last = 0, 10 ** 9
	for ch in line:
		v = value(ch)
		if v > last:
			res -= 2 * last
		res += v
		last = v
	return res

def to_roman(x):
	s = ""

	while x >= 1000:
		s, x = s + "M", x - 1000
	if x >= 900:
		s, x = s + "CM", x - 900
	if x >= 500:
		s, x = s + "D", x - 500
	if x >= 400:
		s, x = s + "CD", x - 400
	
	while x >= 100:
		s, x = s + "C", x - 100
	if x >= 90:
		s, x = s + "XC", x - 90
	if x >= 50:
		s, x = s + "L", x - 50
	if x >= 40:
		s, x = s + "XL", x - 40
	
	while x >= 10:
		s, x = s + "X", x - 10
	if x >= 9:
		s, x = s + "IX", x - 9
	if x >= 5:
		s, x = s + "V", x - 5
	if x >= 4:
		s, x = s + "IV", x - 4
	
	while x >= 1:
		s, x = s + "I", x - 1
	
	return s

with open("data/p089_roman.txt", "r") as in_file:
	ans = 0
	for line in in_file:
		if line[-1] == "\n":
			line = line[:-1]
		ans += len(line) - len(to_roman(to_decimal(line)))
		#print(ans)
	print(ans)
