def getn(a, b, c, d):
	nums = [set() for _ in range(16)]
	nums[1].add(a)
	nums[2].add(b)
	nums[4].add(c)
	nums[8].add(d)

	for cfg in range(1, 16):
		if (cfg & (cfg - 1)) == 0:
			continue

		for cfgl in range(1, cfg):
			if (cfg & cfgl) == cfgl:
				cfgr = cfg ^ cfgl
				
				for vall in nums[cfgl]:
					for valr in nums[cfgr]:
						nums[cfg].add(vall + valr)
						nums[cfg].add(vall - valr)
						nums[cfg].add(vall * valr)
						if abs(valr) > 1e-7:
							nums[cfg].add(vall / valr)
	
	v = set()
	for num in nums[15]:
		if abs(num - round(num)) < 1e-7:
			v.add(round(num))
	
	i = 1
	while i in v:
		i = i + 1
	return i - 1

bst, nums = -1, None
for a in range(10):
	for b in range(a + 1, 10):
		for c in range(b + 1, 10):
			for d in range(c + 1, 10):
				n = getn(a, b, c, d)
				if n > bst:
					bst, nums = n, (a, b, c, d)

print(getn(1, 2, 3, 4))
print(nums)
