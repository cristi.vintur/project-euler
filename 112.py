
def is_bouncy(n):
	up = False
	down = False
	last = n % 10
	nn = n
	n //= 10

	while n > 0 and (not up or not down):
		if n % 10 < last:
			up = True
		elif n % 10 > last:
			down = True

		last = n % 10
		n //= 10

	return up and down

if __name__ == '__main__':

	nr_bouncy = 0
	i = 1

	while True:
		if is_bouncy(i):
			nr_bouncy += 1

		if nr_bouncy / i >= 0.99:
			break

		i += 1

	print(i, nr_bouncy, i)
