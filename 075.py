from math import gcd

# generic solutions:
# a = 2mn
# b = m^2 - n^2
# c = m^2 + n^2
# m > n

VMAX = 1500001
# VMAX = 151
cnt = [0] * VMAX

for i in range(2, VMAX, 2):
	for j in range(i, VMAX, i):
		# i | j
		m, n = i // 2, (j // i) - (i // 2)
		if m > n and n > 0 and gcd(m ** 2 - n ** 2, m ** 2 + n ** 2) == 1:
			cnt[j] += 1

for i in range(VMAX - 1, -1, -1):
	if cnt[i] == 0:
		continue
	
	for j in range(i + i, VMAX, i):
		cnt[j] += cnt[i]

# for i in range(2, 50):
# 	print(i, cnt[i])

print(sum(1 if val == 1 else 0 for val in cnt))
