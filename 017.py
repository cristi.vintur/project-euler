import inflect

p = inflect.engine()

ans = 0
for i in range(1, 1001):
	s = p.number_to_words(i)
	ans += sum(1 for ch in s if ('a' <= ch and ch <= 'z'))

print(ans)
