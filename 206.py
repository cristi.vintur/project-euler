from queue import Queue

def matches_pattern(l, n, pattern):
	if l > len(pattern):
		return False

	for i in range(len(pattern) - 1, len(pattern) - l - 1, -1):
		if pattern[i] != '*' and ord(pattern[i]) - ord('0') != n % 10:
			return False

		n //= 10

		if n == 0:
			break

	return True

if __name__ == '__main__':

	pattern = "1*2*3*4*5*6*7*8*9*0"

	q = Queue()

	q.put((0, 0))

	while True:
		v, l = q.get_nowait()
		# print(v, l)

		for d in range(10):
			newv = d * (10 ** l) + v
			if matches_pattern(l + 1, newv * newv, pattern):
				if len(str(newv * newv)) == len(pattern):
					if matches_pattern(len(pattern), newv * newv, pattern):
						print(newv)
						exit(0)
				elif l < 11:
					q.put((newv, l + 1))
