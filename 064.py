from math import sqrt, gcd
from util_math import get_continued_fraction

ans = 0
for n in range(2, 10001):
	sqrtn = int(sqrt(n))
	if n != sqrtn * sqrtn:
		if len(get_continued_fraction(n)) % 2 == 0:
			ans += 1

print(ans)
