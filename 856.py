import numpy as np

def update(dp, nr_cards, nr, nxt, p):
	nr[nxt] -= 1
	if nxt > 0:
		nr[nxt - 1] += 1

	# if nr[1] < 14 and nr[2] < 14 and nr[3] < 14:
	# 	dp[nr_cards][nr[1]][nr[2]][nr[3]][4 if nxt == 0 else nxt - 1] += p
	if dp[nr_cards][nr[1]][nr[2]][nr[3]][4 if nxt == 0 else nxt - 1] < 0:
		dp[nr_cards][nr[1]][nr[2]][nr[3]][4 if nxt == 0 else nxt - 1] = 0

	dp[nr_cards][nr[1]][nr[2]][nr[3]][4 if nxt == 0 else nxt - 1] += p

if __name__ == '__main__':
	
	dp = np.zeros((53, 14, 14, 14, 5), dtype = np.longdouble)
	dp.fill(-1)

	ans = 0
	dp[52][0][0][13][4] = 1.0

	for nr_cards in range(52, -1, -1):
		# print(nr_cards)
		for b in range(13, -1, -1):
			for c in range(13, -1, -1):
				for d in range(13, -1, -1):
					a = nr_cards - 2 * b - 3 * c - 4 * d
					if a < 0 or a > 13:
						continue

					for last in range(0, 5):
						curr_prob = dp[nr_cards][b][c][d][last]
						if curr_prob < 0:
							continue

						if nr_cards == 0:
							ans += curr_prob * 52

						for nxt in range(0, 4):
							nr = [a, b, c, d]
							
							if nr[nxt] == 0:
								continue

							if nxt == last:
								p_win = (nxt + 1) / nr_cards
								p_continue = (nxt + 1) * (nr[nxt] - 1) / nr_cards

								ans += curr_prob * p_win * (53 - nr_cards)
								# if curr_prob > 0:
								# 	print(a, b, c, d, last, nxt, curr_prob, p_win)

								update(dp, nr_cards - 1, nr, nxt, curr_prob * p_continue)
							else:
								p_continue = (nxt + 1) * nr[nxt] / nr_cards
								update(dp, nr_cards - 1, nr, nxt, curr_prob * p_continue)

	print(ans)
