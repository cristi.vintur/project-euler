ans = 0
prob = []

prob.append({})
prob[0][(1, 0, 0, 0, 0)] = 1.0

for i in range(16):
	prob.append({})
	for v, p in prob[i].items():
		if i > 0 and i < 15 and sum(v) == 1:
			print(i, v, p)
			ans += p
		
		for j in range(len(v)):
			choose_prob = v[j] / sum(v)
			if choose_prob > 0:
				newv = (0, )
				for k in range(1, len(v)):
					if k < j:
						newv += (v[k], )
					elif k == j:
						newv += (v[k] - 1, )
					else:
						newv += (v[k] + 1, )
				
				if newv in prob[i + 1]:
					prob[i + 1][newv] += choose_prob * p
				else:
					prob[i + 1][newv] = choose_prob * p

print(ans)