#include <bits/stdc++.h>
using namespace std;
using uint = unsigned int;
using ll = long long;
using ull = unsigned long long;
using pii = pair<int, int>;
using pll = pair<ll, ll>;
#define dbg(x) cerr<<#x": "<<(x)<<endl
#define dbg_p(x) cerr<<#x": "<<(x).first<<' '<<(x).second<<endl
#define dbg_v(x, n) {cerr<<#x"[]: ";for(long long _=0;_<n;++_)cerr<<(x)[_]<<' ';cerr<<endl;}
#define all(v) v.begin(), v.end()
#define fi first
#define se second

template<typename T1, typename T2>
ostream& operator <<(ostream &out, const pair<T1, T2> &item) {
	out << '(' << item.first << ", " << item.second << ')';
	return out;
}

template <typename T>
ostream& operator <<(ostream &out, const vector<T>& v) {
	for(const auto &item : v) out << item << ' ';
	return out;
}

const int MOD = 1000000;

inline int f(int k) {
	return k * (3 * k - 1) / 2;
}

int main()
{
	ios_base::sync_with_stdio(false);
	
	vector<int> dp;

	dp.push_back(1);
	dp.push_back(1);

	for(int i = 2; ; ++i) {
		int x = 0;
		for(int k = 1; ; ++k) {
			int a = f(k);
			int b = f(-k);

			if(a <= i) x += ((k & 1) ? 1 : -1) * dp[i - a];
			if(b <= i) x += ((k & 1) ? 1 : -1) * dp[i - b];
			else break;

			while(x >= MOD) x -= MOD;
			while(x < 0) x += MOD;
		}

		dp.push_back(x % MOD);
		if(i == 5) cout << dp.back() << '\n';
		if(dp.back() == 0) return cout << i << '\n', 0;
	}

	return 0;
}
