x, y = 3, 2
ans = 0

# does not count first expansion
for i in range(999):
	x, y = x + 2 * y, x + y
	if len(str(x)) > len(str(y)):
		ans += 1

print(ans)