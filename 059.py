def decode(encrypted, key):
	decrypted = []
	key = list(map(ord, key))
	for i in range(len(encrypted)):
		decrypted.append(encrypted[i] ^ key[i % len(key)])
	
	return ''.join(list(map(chr, decrypted)))

def ok(s):
	if max(list(map(len, s.split()))) >= 30:
		return False
	
	return True

with open("p059_cipher.txt") as f:
	v = list(map(int, f.read().split(',')))

alphabet = ''.join(list(map(lambda x : chr(ord('a') + x), range(0, 26))))
for ch1 in alphabet:
	for ch2 in alphabet:
		for ch3 in alphabet:
			s = decode(v, ch1 + ch2 + ch3)
			if ok(s):
				print(ch1 + ch2 + ch3)
				print(s)
				print(sum(map(ord, s)))
				foo = input()

# key = 'exp'