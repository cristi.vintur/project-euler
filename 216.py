from util_math import sieve

def f(v, n, p):
	if v[n] % p != 0:
		print("err", n, v[n], p)
	while v[n] % p == 0:
		v[n] //= p

n = 1 + int(input())

v = [2 * i * i - 1 for i in range(n)]
print("start")
for i in range(2, n):
	if i <= 1000 or i % 100000 == 0:
		print("i", i)
	
	if v[i] == 1:
		continue
	
	j = 1
	while -i + j * v[i] < n:
		if -i + j * v[i] > i:
			f(v, -i + j * v[i], v[i])
		if i + j * v[i] < n:
			f(v, i + j * v[i], v[i])
		j += 1

ans = 0
for i in range(2, n):
	if v[i] == 2 * i * i - 1:
		ans += 1

print("ans", ans)

#is_prime_ok, primes = sieve(2 * n * n)
#for i in range(2, n + 1):
#	if is_prime[i] != is_prime_ok[2 * i * i - 1]:
#		print(i)
#		exit(0)
