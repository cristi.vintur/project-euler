if __name__ == '__main__':
	
	n = 50
	lengths = [2, 3, 4]

	dp = [0] * (n + 1)
	dp[0] = 1

	for i in range(1, n + 1):
		dp[i] = dp[i - 1]
		for l in lengths:
			if l <= i:
				dp[i] += dp[i - l]

	print(dp[n])
