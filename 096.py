from copy import deepcopy

def update(d, key, val):
	#print(d)
	#print(key)
	#print(val)
	if key in d:
		del d[key]
	
	for cell, pos in d.items():
		if cell[0] == key[0] or cell[1] == key[1] or (cell[0] // 3 == key[0] // 3 and cell[1] // 3 == key[1] // 3):
			if val in pos:
				pos.remove(val)
	
	for cell, pos in list(d.items()):
		if not pos:
			return False
	
	#print(d, end = "\n\n")
	return True

def check(v):
	for i in range(9):
		for j in range(9):
			for k in range(j + 1, 9):
				if v[i][j] == v[i][k] or v[j][i] == v[k][i]:
					return False
	return True

def bkt(v, pos_dict):
	while pos_dict:
		i, j, pos = -1, -1, None
		for cell, cell_pos in pos_dict.items():
			if pos == None or len(pos) > len(cell_pos):
				i, j, pos = cell[0], cell[1], cell_pos

		if len(pos) == 1:
			v[i][j] = next(iter(pos))
			if not update(pos_dict, (i, j), next(iter(pos))):
				return None
		else:
			break
	
	if pos_dict:
		for val in pos:
			new_pos_dict = deepcopy(pos_dict)
			v[i][j] = val
			if not update(new_pos_dict, (i, j), val):
				continue

			ret = bkt(v, new_pos_dict)
			if ret != None:
				return ret

		return None
	else:
		# if not check(v):
		# 	assert False
		return v[0][0] * 100 + v[0][1] * 10 + v[0][2]

with open("data/p096_sudoku.txt", "r") as in_file:
	v = [[0] * 9 for _ in range(9)]
	ans = 0

	for t in range(50):
		d = {}
		for i in range(9):
			for j in range(9):
				d[(i, j)] = set(range(1, 10))
		
		header = in_file.readline()
		for i in range(9):
			s = in_file.readline()
			for j in range(9):
				v[i][j] = ord(s[j]) - ord('0')
				if v[i][j] > 0:
					update(d, (i, j), v[i][j])
	
		ans += bkt(v, d)
	
	print(ans)

