from math import sqrt

def int_sqrt(x):
	l, r = 1, x
	while l < r:
		mid = (l + r + 1) // 2
		if mid * mid <= x:
			l = mid
		else:
			r = mid - 1
	return l

ans = 0

for i in range(2, 100):
	if i in [4, 9, 16, 25, 36, 49, 64, 81]:
		continue
	
	x = i * (10 ** (2 * 100))
	y = int_sqrt(x)
	s = str(y)[:100]
	#print(s)
	ans += sum([int(d) for d in s])

print(ans)
