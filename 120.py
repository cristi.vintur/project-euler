def solve(a):
	# if n = 0 (mod 2), then r = 2
	# if n = 1 (mod 2), then r = 2 * n * a % (a^2)
	res = 0
	for n in range(1, 10 * a):
		r = (pow(a - 1,  n, a * a) + pow(a + 1, n, a * a)) % (a * a)
		# if r == 922560:
		# 	print(n)
		res = max(res, r)
	return res

def solve2(a):
	if a % 2 == 1:
		return max([2, 2 * (a // 2) * a, (2 * (a - 1) * a) % (a * a)])
	else:
		largest_odd_until_half = ((a - 1) // 2) + ((a - 1) // 2) % 2 - 1
		largest_odd_after_half = a - 1 + (a - 1) % 2 - 1
		return max([2, 2 * largest_odd_until_half * a, (2 * largest_odd_after_half * a) % (a * a)])

if __name__ == '__main__':
	ans = 0

	for a in range(3, 1001):
		ans += solve2(a)

	print(ans)
