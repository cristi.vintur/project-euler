def solve(n, m):

	dp = [0] * (n + 1)
	dp[0] = 1

	for i in range(1, n + 1):
		dp[i] = dp[i - 1] + (dp[i - m] if m <= i else 0)

	# subtracting the solution without any tiles
	return dp[n] - 1

if __name__ == '__main__':

	n = 50
	print(solve(n, 2) + solve(n, 3) + solve(n, 4))
