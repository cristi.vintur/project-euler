from math import sqrt

# cuboid with sides a <= b <= c
# shortest route: sqrt((a + b)^2 + c^2)

def is_square(n):
	sq = int(sqrt(n))
	return sq * sq == n

# return the number of pairs (x, y)
# x + y == s, 1 <= x <= y <= m
def get(s, m):
	# s - x <= m <=> x >= s - m
	mn, mx = max(1, s - m), s // 2
	return 0 if mn > mx else mx - mn + 1

LIM = 10 ** 6

nr_sol = 0

m = 1
while True:
	for s in range(1, 2 * m + 1):
		if is_square(s ** 2 + m ** 2):
			nr_sol += get(s, m)
	
	if nr_sol >= LIM:
		print(m)
		exit(0)

	#print(nr_sol, m)
	m += 1
