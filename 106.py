from itertools import combinations

def need_test(a, b):
	lt, gt = False, False
	for x, y in zip(a, b):
		if x < y:
			lt = True
		else:
			gt = True
	return (lt and gt)

n = int(input())

nums = list(range(1, n + 1))
ans = 0
for nr in range(1, n):
	for lhs in combinations(nums, nr):
		rem = list(filter(lambda x : x not in lhs, nums))
		for rhs in combinations(rem, nr):
			if need_test(lhs, rhs):
				ans += 1

print(ans // 2)

