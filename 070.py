def ok(x, y):
	return sorted(list(str(x))) == sorted(list(str(y)))

def get_phi(n):
	phi = list(range(0, n))
	
	for i in range(2, n):
		if phi[i] != i:
			continue
		
		for j in range(i, n, i):
			phi[j] = phi[j] // i * (i - 1)
	
	return phi

phi = get_phi(10 ** 7)

n, vmax = 2, 10 ** 18
for i in range(3, 10 ** 7):
	if 1.0 * i / phi[i] < vmax and ok(i, phi[i]):
		n, vmax = i, 1.0 * i / phi[i]

print(n)
