from itertools import combinations

def norm(s):
	if 6 in s:
		s.add(9)
	if 9 in s:
		s.add(6)

digits = list(range(10))

ans = 0
for l1 in combinations(digits, 6):
	for l2 in combinations(digits, 6):
		s1, s2 = set(l1), set(l2)
		norm(s1)
		norm(s2)

		ok = True
		for i in range(1, 10):
			d1, d2 = (i * i) // 10, (i * i) % 10
			if ((d1 not in s1) or (d2 not in s2)) and ((d1 not in s2) or (d2 not in s1)):
				ok = False

		if ok:
			ans += 1

print(ans // 2)
