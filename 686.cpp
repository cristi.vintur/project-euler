#include <bits/stdc++.h>
using namespace std;
using uint = unsigned int;
using ll = long long;
using ld = long double;
using ull = unsigned long long;
using pii = pair<int, int>;
using pll = pair<ll, ll>;
#define dbg(...) cerr << #__VA_ARGS__ << " ->", debug_out(__VA_ARGS__)
#define dbg_p(x) cerr<<#x": "<<(x).first<<' '<<(x).second<<endl
#define dbg_v(x, n) {cerr<<#x"[]: ";for(long long _=0;_<n;++_)cerr<<(x)[_]<<' ';cerr<<endl;}
#define all(v) v.begin(), v.end()
#define fi first
#define se second
void debug_out() { cerr << endl; }
template <typename Head, typename... Tail> void debug_out(Head H, Tail... T) { cerr << " " << H; debug_out(T...);}

template<typename T1, typename T2>
ostream& operator <<(ostream &out, const pair<T1, T2> &item) {
	out << '(' << item.first << ", " << item.second << ')';
	return out;
}

template <typename T>
ostream& operator <<(ostream &out, const vector<T>& v) {
	for(const auto &item : v) out << item << ' ';
	return out;
}

const int IDX = 678910;
const int PREF = 123;

int main()
{
	ios_base::sync_with_stdio(false);
	
	int dpref = 0;
	for(int p10 = 1; p10 <= PREF; p10 *= 10) ++dpref;

	ld lgLow = log10(PREF);
	ld lgUpp = log10(PREF + 1);
	ld lg2 = log10(2);

	int k, nr;
	for(nr = 0, k = 5; nr < IDX; ++k) {
		int d = int(floor(k * lg2)) + 1 - dpref;

		//if(k == 7) dbg(lgLow + d, k * lg2, lgUpp + d, d);
		if(lgLow + d <= k * lg2 && k * lg2 < lgUpp + d) ++nr;
	}

	cout << k - 1 << '\n';

	return 0;
}

