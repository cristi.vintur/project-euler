from queue import PriorityQueue
from copy import deepcopy

MOD = 1234567891
PHI_MOD = MOD - 1 # MOD is prime

n, m = 10 ** 4, 10 ** 16
#n, m = 10, 100
#n, m = 5, 3

q = PriorityQueue()

for i in range(2, n + 1):
	q.put(i)

mx = n
while True:
	m -= 1

	x = q.get()
	q.put(x * x)

	if x * x > n or m == 0:
		break

#while not q.empty():
#	x = q.get()
#	print(x)

complete_rounds = m // (n - 1)

q2 = PriorityQueue()
while not q.empty():
	x = q.get()
	q2.put((x, pow(x, pow(2, complete_rounds, PHI_MOD), MOD)))

m -= complete_rounds * (n - 1)

ans = 0
for i in range(m):
	x, x_mod = q2.get()
	ans += (x_mod * x_mod) % MOD

while not q2.empty():
	x, x_mod = q2.get()
	ans += x_mod

print(ans % MOD)

