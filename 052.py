def ok(x):
	s = sorted(list(str(2 * x)))
	for i in range(3, 7):
		if s != sorted(list(str(i * x))):
			return False
	
	return True

for lg in range(15):
	for i in range(10 ** lg, 10 ** (lg + 1)):
		if 6 * i >= (10 ** (lg + 1)):
			break
		
		if ok(i):
			print(i)
			exit(0)