import numpy as np

def get(v, i):
	if i < 0 or i >= len(v):
		return 0

	return v[i]

if __name__ == '__main__':
	
	m = 50

	dp = [1] * m
	s = m

	# sum of previous m elements, except last one
	s_last_m_1 = m - 1

	i = m
	while True:
		dp.append(s - s_last_m_1 + 1)

		s += dp[i]
		s_last_m_1 += dp[i - 1] - dp[i - m]

		if dp[i] >= 10 ** 6:
			break

		i += 1

	print(len(dp) - 1)
