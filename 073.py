from math import gcd

ans = 0
for d in range(2, 12001):
	if d % 1000 == 0:
		print(d)
	
	for n in range(1, d):
		if gcd(n, d) == 1:
			if 2 * n < d:
				if 3 * n > d:
					ans += 1
			else:
				break

print("ans", ans)
