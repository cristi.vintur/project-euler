#include <bits/stdc++.h>
using namespace std;
using uint = unsigned int;
using ll = long long;
using ld = long double;
using ull = unsigned long long;
using pii = pair<int, int>;
using pll = pair<ll, ll>;
#define dbg(x) cerr<<#x": "<<(x)<<endl
#define dbg_p(x) cerr<<#x": "<<(x).first<<' '<<(x).second<<endl
#define dbg_v(x, n) {cerr<<#x"[]: ";for(long long _=0;_<n;++_)cerr<<(x)[_]<<' ';cerr<<endl;}
#define all(v) v.begin(), v.end()
#define fi first
#define se second

template<typename T1, typename T2>
ostream& operator <<(ostream &out, const pair<T1, T2> &item) {
	out << '(' << item.first << ", " << item.second << ')';
	return out;
}

template <typename T>
ostream& operator <<(ostream &out, const vector<T>& v) {
	for(const auto &item : v) out << item << ' ';
	return out;
}

const int V = 100000001;
//const int V = 5;
const int MOD = 1000000009;

bool isPrime[V];

int pw(int base, int exp, int mod = MOD) {
	int res;
	for(res = 1; exp; exp >>= 1) {
		if(exp & 1) res = (1LL * res * base) % mod;
		base = (1LL * base * base) % mod;
	}
	
	return res;
}

int modInv(int base, int mod) {
	return pw(base, mod - 2, mod);
}

int getExp(int p, int n) {
	int res;
	for(res = 0; n; n /= p) res += n / p;
	return res;
}

int main()
{
	ios_base::sync_with_stdio(false);
	
	for(int i = 2; i < V; ++i) isPrime[i] = i % 2;
	isPrime[2] = true;

	for(int i = 3; i * i < V; i += 2) {
		if(!isPrime[i]) continue;
		for(int j = i * i; j < V; j += i + i) isPrime[j] = false;
	}

	int ans = 1;
	for(int i = 2; i < V; ++i)
		if(isPrime[i]) ans = (1LL * ans * (1 + pw(i, 2 * getExp(i, V - 1)))) % MOD;

	cout << ans << '\n';

	return 0;
}
