from math import floor

# returns kth decimal of 1/n
def kth_decimal(n, k):
	r = (pow(10, k - 1, n) / n)
	return floor(10 * r) % 10

#print(kth_decimal(3, 7))
#print(kth_decimal(6, 7))
#print(kth_decimal(7, 7))

def s(n):
	res = 0
	for i in range(2, n + 1):
		#res += kth_decimal(i, n)
		res += kth_decimal(i, n)
	return res

print(s(10 ** 7))

