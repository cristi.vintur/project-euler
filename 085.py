N = 1000
INF = 5 * (10 ** 6)

dp = []
# nr_rect[i][j] = number of rectangles on grid i x j

dp.append([0] * N)

bst, area = INF, 0
for i in range(1, N):
	dp.append([0] * N)
	for j in range(0, N):
		dp[i][j] = dp[i - 1][j] + j * (j + 1) // 2 * i
		if dp[i][j] >= INF:
			dp[i][j] = INF

		if abs(dp[i][j] - 2 * (10 ** 6)) < bst:
			bst, area = abs(dp[i][j] - 2 * (10 ** 6)), i * j

print(area)
