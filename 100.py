LIM = 10 ** 12

# equation: n(n - 1) = 2b(b - 1)
# equivalent: x^2 - 2y^2 = -1, x = 2n - 1, y = 2b - 1

# (A, B) -> smallest solution of u^2 - 2v^2 = -1
# (u, v) -> solution of u^2 - 2v^2 = 1
# (x, y) -> solution of x^2 - 2y^2 = -1

A, B = 1, 1
u0, v0 = 3, 2
u, v = u0, v0

while True:
	assert u * u - 2 * v * v == 1
	x, y = B * u + 2 * A * v, A * u + B * v
	n, b = (x + 1) // 2, (y + 1) // 2
	assert 2 * b * (b - 1) == n * (n - 1)
	if n >= LIM:
		print("n =", n)
		print("b =", b)
		break
	
	u, v = u0 * u + 2 * v0 * v, v0 * u + u0 * v
