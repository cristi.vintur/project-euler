def getLpd(n):
	lpd = list(range(n))

	for i in range(2, n):
		if lpd[i] == i:
			for j in range(i, n, i):
				lpd[j] = min(lpd[j], i)
	
	return lpd

def getDivisorSum(n, lpd):
	divSum = [0] * n
	divSum[1] = 1
	for i in range(2, n):
		p, pw, ii = lpd[i], 1, i
		while ii % p == 0:
			ii //= p
			pw *= p

		divSum[i] = divSum[ii] * (pw * p - 1) // (p - 1)
	
	return divSum

def go(n, vis, nxt, t):
	stk = []
	while not vis[n]:
		stk.append(n)
		vis[n] = t
		n = nxt[n]
		if n >= len(vis):
			return -1, -1
	
	if vis[n] != t:
		return -1, -1
	
	val, lg, minv = n, 1, n
	while stk[-1] != n:
		lg += 1
		minv = min(minv, stk[-1])
		del stk[-1]
	
	return lg, minv

N = 10 ** 6
#N = 300

lpd = getLpd(N)
nxt = getDivisorSum(N, lpd)
for i in range(1, N):
	nxt[i] -= i

vis = [0] * N

lgmax, ans = 0, 0
t = 0
for i in range(2, N):
	if vis[i] == 0:
		t += 1
		lg, minv = go(i, vis, nxt, t)
		if lg > lgmax:
			lgmax, ans = lg, minv

print("length:", lgmax)
print("min value:", ans)

