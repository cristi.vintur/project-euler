from util_math import is_prime, sieve

V = 10 ** 6

def is_prime_with_cache(n, is_prime_vec):
	if n < len(is_prime_vec):
		return is_prime_vec[n]

	return is_prime(n)

def bkt(last, curr, digits_used, used, is_prime_vec):
	# if digits_used == 2:
	# 	print(curr, digits_used, used)
	
	if digits_used == 9:
		return 1 if is_prime_with_cache(curr, is_prime_vec) and last < curr else 0

	curr_is_prime = True if curr > 0 and is_prime_with_cache(curr, is_prime_vec) else False

	ans = 0
	for d in range(1, 10):
		if not used[d]:
			used[d] = True

			if curr_is_prime and last < curr:
				ans += bkt(curr, d, digits_used + 1, used, is_prime_vec)

			if last != 0 or curr < 10 ** 4:
				ans += bkt(last, curr * 10 + d, digits_used + 1, used, is_prime_vec)

			used[d] = False

	return ans

if __name__ == '__main__':

	is_prime_vec, primes = sieve(V)

	used = [False] * 10

	nr = bkt(0, 0, 0, used, is_prime_vec)

	# 44680
	print(nr)
