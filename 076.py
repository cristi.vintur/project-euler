def count_ways_to_write_as_sum(n):
	dp = [0] * (n + 1)
	dp[0] = 1
	
	for i in range(1, n + 1):
		j = 1
		while j * (3 * j - 1) // 2 <= i:
			dp[i] += dp[i - j * (3 * j - 1) // 2] * (1 if j % 2 == 1 else -1)
			
			if j < 0:
				j = 1 - j
			else:
				j = -j
	
	return dp

print(count_ways_to_write_as_sum(100)[-1] - 1)
