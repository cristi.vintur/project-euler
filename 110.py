from util_math import sieve
from heapq import heappush, heappop

nr_sol = int(input())

sz = 0
def push(q, n, p, lst, l):
	global sz
	if (p + 1) // 2 > nr_sol:
		print(n)
		print("sz", sz)
		exit(0)
	
	sz += 1
	heappush(q, (n, p, lst, l))

is_prime, primes = sieve(200)

q = []
heappush(q, (1, 1, 0, []))

while True:
	n, p, lst, l = heappop(q)
	for i in range(lst, len(l)):
		if i == 0 or l[i - 1] > l[i]:
			newp = p // (2 * l[i] + 1) * (2 * l[i] + 3)
			newl = l.copy()
			newl[i] += 1
			push(q, n * primes[i], newp, i, newl)
	
	newp = p * 3
	newl = l.copy()
	newl.append(1)
	push(q, n * primes[len(l)], newp, len(newl) - 1, newl)
