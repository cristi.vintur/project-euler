from bisect import bisect_left

VMAX = (10 ** 6) + 10

is_prime = [True] * VMAX
primes = []
s = [0]

for i in range(2, VMAX):
	if not is_prime[i]:
		continue
	
	primes.append(i)
	s.append(s[-1] + i)
	for j in range(i * i, VMAX, i):
		is_prime[j] = False

ans = 0
for lg in range(21, 1000):
	print(lg)
	for i in range(len(primes) - lg + 1):
		p = s[i + lg] - s[i]
		i = bisect_left(primes, p)
		if i != len(primes) and primes[i] == p:
			ans = p
	
	print("ans", ans)

print("ans", ans)