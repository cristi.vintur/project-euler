#include <bits/stdc++.h>
using namespace std;
using uint = unsigned int;
using ll = long long;
using ld = long double;
using ull = unsigned long long;
using pii = pair<int, int>;
using pll = pair<ll, ll>;
#define dbg(...) cerr << #__VA_ARGS__ << " ->", debug_out(__VA_ARGS__)
#define dbg_p(x) cerr<<#x": "<<(x).first<<' '<<(x).second<<endl
#define dbg_v(x, n) {cerr<<#x"[]: ";for(long long _=0;_<n;++_)cerr<<(x)[_]<<' ';cerr<<endl;}
#define all(v) v.begin(), v.end()
#define fi first
#define se second
void debug_out() { cerr << endl; }
template <typename Head, typename... Tail> void debug_out(Head H, Tail... T) { cerr << " " << H; debug_out(T...);}

template<typename T1, typename T2>
ostream& operator <<(ostream &out, const pair<T1, T2> &item) {
	out << '(' << item.first << ", " << item.second << ')';
	return out;
}

template <typename T>
ostream& operator <<(ostream &out, const vector<T>& v) {
	for(const auto &item : v) out << item << ' ';
	return out;
}

const int N = 100010;
const int MOD = 1000000007;

int x[3], y[3];
int dp[N][3][3][3][3];

void add(int &dp, int nr, int x) {
	dp = (dp + 1LL * nr * x) % MOD;
}

int main()
{
	ios_base::sync_with_stdio(false);
	
	int n = 100000;

	dp[0][1][0][0][0] = 1;
	for(int i = 0; i < n; ++i)
		for(x[0] = 0; x[0] < 3; ++x[0])
			for(x[1] = 0; x[1] < 3; ++x[1])
				for(x[2] = 0; x[2] < 3; ++x[2])
					for(int s = 0; s < 3; ++s)
						for(int d = 0; d < 3; ++d) {
							memcpy(y, x, sizeof x);
							(++y[(s + d) % 3]) %= 3;

							int nr = 3 + (d == 0 && i > 0);
							add(dp[i + 1][y[0]][y[1]][y[2]][(s + d) % 3], nr, dp[i][x[0]][x[1]][x[2]][s]);
						}

	int ans = 0;
	for(int x = 0; x < 3; ++x)
		for(int y = 0; y < 3; ++y)
			for(int z = 0; z < 3; ++z)
				for(int s = 0; s < 3; ++s)
					if((x != 2 && y != 2 && z != 2) || (x == 2 && y == 2 && z == 2))
						ans = (ans + dp[n][x][y][z][s]) % MOD;
	
	cout << ans << '\n';

	return 0;
}

